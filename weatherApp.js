const apikey = "53226fa94906cd009e4de9c5cdd6cbf1"
const apiurl = "https://api.openweathermap.org/data/2.5/weather?&units=metric&q=";
const weatherIcon = document.querySelector(".weather-icon")
const searchbox = document.querySelector(".search input")
const searchbtn = document.querySelector(".search button")
//
async function checkWeather(city) {

    const response = await fetch(apiurl + city + `&appid=${apikey}`);
    if (response.status == 404) {
        document.querySelector(".error").style.display = "block"
        document.querySelector(".weather").style.display = "none"
    }



    else {
        var data = await response.json();
        console.log(data)

        document.querySelector(".city").innerHTML = data.name;
        document.querySelector(".temp").innerHTML = Math.round(data.main.temp) + "•c";
        document.querySelector(".humidity").innerHTML = data.main.humidity + " %";
        document.querySelector(".windy").innerHTML = data.wind.speed + " km/hr";
        if (data.weather[0].main == "Clouds") {
            weatherIcon.src = "images/cloudy-weather.jpg";
        }
        else if (data.weather[0] == "clear") {
            weatherIcon.src = "/images/weather.jpg"
        }
        else if (data.weather[0] == "rain") {
            weatherIcon.src = "/images/sunny.jpg"
        }
        else if (data.weather[0] == "rain") {
            weatherIcon.src = "/images/weather.jpg"
        }
        else if (data.weather[0] == "drizzle") {
            weatherIcon.src = "/images/sunny.jpg"
        }
        else if (data.weather[0] == "mist") {
            weatherIcon.src = "/images/sunny.jpg"
        }
        document.querySelector(".error").style.display = "none"
    }
}
searchbtn.addEventListener("click", () => {
    checkWeather(searchbox.value);
})
